# Hummingbird zsh theme
based on gnzh whih is itself based on bira ʕ •̀ ω •́ ʔ

![screenshot](https://gitlab.com/hummird/hummingbird/raw/master/img/screenshot.png)
## Install

1. clone this repo: ```git clone https://gitlab.com/hummird/hummingbird```;
2. copy theme: ```cp ./hummingbird/hummingbird.zsh-theme $ZSH/themes/passion.zsh-theme```;
3. modify your rc: open ```~/.zshrc``` find ```ZSH_THEME``` edit to ```ZSH_THEME="hummingbird"```;
4. apply rc: ```source ~./zshrc```;

* see also: [Overriding and adding themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Customization#overriding-and-adding-themes);

