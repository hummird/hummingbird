# Mostly based on gnzh theme

setopt prompt_subst

() {

local PR_USER PR_USER_OP PR_PROMPT PR_HOST

# Check the UID
if [[ $UID -ne 0 ]]; then # normal user
  PR_USER='%F{green}%n%f'
  PR_USER_OP='%F{green}%#%f'
  PR_PROMPT='%f➤ %f'
else # root
  PR_USER='%F{red}%n%f'
  PR_USER_OP='%F{red}%#%f'
  PR_PROMPT='%F{red}➤ %f'
fi

# Check if we are on SSH or not
if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
  PR_HOST='%F{red}@%M%f' # SSH
else
  PR_HOST='' # no SSH
fi

_newline=$'\n'
_lineup=$'\e[1A'
_linedown=$'\e[1B'

local time="%F{green}[%t]%f"
local return_code="%(?.⎨%F{blue}%?↩%f⎬.⎨%F{blue}%?↩%f⎬)"

local user="${PR_USER}"
local host="${PR_HOST}"
local current_dir="%B%F{blue}%~%f%b"
local git_branch='$(git_prompt_info)'

PROMPT="╭─${user}${host} ${current_dir} \$(ruby_prompt_info) ${git_branch} 
╰─${return_code}─$PR_PROMPT "
RPROMPT="%{${_lineup}%}${time}%{${_linedown}%}"

ZSH_THEME_GIT_PROMPT_PREFIX="%F{yellow}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %f"
ZSH_THEME_RUBY_PROMPT_PREFIX="%F{red}‹"
ZSH_THEME_RUBY_PROMPT_SUFFIX="›%f"

}
